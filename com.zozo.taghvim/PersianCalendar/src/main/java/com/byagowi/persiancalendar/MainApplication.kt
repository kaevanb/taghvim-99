package com.byagowi.persiancalendar

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.byagowi.persiancalendar.utils.initUtils
import me.cheshmak.android.sdk.core.Cheshmak
import me.cheshmak.cheshmakplussdk.core.CheshmakPlus

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
       // ReleaseDebugDifference.mainApplication(this)
        initUtils(applicationContext)

        Cheshmak.with(applicationContext)
        Cheshmak.initTracker("***")
        CheshmakPlus.with(applicationContext)


    }


}
