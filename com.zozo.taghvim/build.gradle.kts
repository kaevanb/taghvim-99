// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath ("com.google.gms:google-services:4.3.3")
        classpath("com.android.tools.build:gradle:3.6.2")
        classpath(kotlin("gradle-plugin", version = "1.3.72"))
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven("https://jitpack.io")
maven("https://repository.cheshmak.me")

           
    }
}

task("clean") {
    delete(rootProject.buildDir)
}
